import tweepy
consumer_key ='ENTER YOUR KEY HERE'
consumer_secret = 'ENTER YOUR SECRET HERE'
access_token = 'ENTER YOUR TOKEN HERE'
access_token_secret = 'ENTER YOUR SECRET HERE'
# Creating the authentication object
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
# Setting your access token and secret
auth.set_access_token(access_token, access_token_secret)
# Creating the API object while passing in auth information
api = tweepy.API(auth)

def myTimeline():

    # Using the API object to get tweets from your timeline, and storing it in a variable called public_tweets
    public_tweets = api.home_timeline()
    # foreach through all tweets pulled
    for tweet in public_tweets:
        # printing the text stored inside the tweet object
        print (tweet.text)

myTimeline()

def getUserTweets():
    # Creating the API object while passing in auth information
    api = tweepy.API(auth)

    # The Twitter user who we want to get tweets from
    name = "nytimes"
    # Number of tweets to pull
    tweetCount = 20

    # Calling the user_timeline function with our parameters
    results = api.user_timeline(id=name, count=tweetCount)

    # foreach through all tweets pulled
    for tweet in results:
        # printing the text stored inside the tweet object
        print (tweet.text)
getUserTweets()
